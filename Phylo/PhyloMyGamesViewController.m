//
//  PhyloMyGamesViewController.m
//  Phylo
//
//  Created by Cody on 2013-06-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloMyGamesViewController.h"

@interface PhyloMyGamesViewController ()

@end

@implementation PhyloMyGamesViewController
@synthesize gamesTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"My Games", @"My Games");
        self.tabBarItem.image = [UIImage imageNamed:@"first"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

//Customize the number of rows in Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *sectionName;
    switch(section){
        case 0:
            sectionName = NSLocalizedString(@"My current games", @"My current games");
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

//Customize the appearance of table view cells
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    //set up the cell
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    
    if([indexPath row] == 0)
        cell.textLabel.text = [NSString stringWithFormat:@"Fun @ SFU Burnaby"];
    else if ([indexPath row] == 1)
        cell.textLabel.text = [NSString stringWithFormat:@"SFU Scavenger Hunt :)"];
    else if ([indexPath row] == 2)
        cell.textLabel.text = [NSString stringWithFormat:@"Surrey Campus Hunting"];
    
    return cell;
}

@end
