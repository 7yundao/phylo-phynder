//
//  PhyloPublicGamesViewController.h
//  Phylo
//
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloPublicGamesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UISearchBar* search;
    IBOutlet UITableView* searchesTable;
}

@property (nonatomic, retain) UISearchBar* search;
@property (nonatomic, retain) UITableView* searchesTable;



@end
