//
//  PhyloQuiz.h
//  Phylo
//
//  Created by Yun Zeng on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhyloQuiz : NSObject
@property (nonatomic, retain) NSString *Question;
@property (nonatomic, retain) NSString *Answer;
@property (nonatomic, retain) NSMutableArray *wrongAnswer;
@property (nonatomic) int card;

- (void) setAnswer:(NSString *)Answer;
- (void) setQuestion:(NSString *)Question;
- (void) setWrongAnswer:(NSMutableArray *)wrongAnswer;
- (void) setCard:(int)card;

+ (NSString*) getAnswer;
+ (NSString*) getQuestion;
+ (NSMutableArray*) getwrongAnswer;
+ (int) getCard;

@end
