//
//  PhyloQuiz.m
//  Phylo
//
//  Created by Yun Zeng on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloQuiz.h"

@implementation PhyloQuiz

@synthesize Question;
@synthesize Answer;
@synthesize wrongAnswer;
@synthesize card;

- (id) init
{
    self = [super init];
    if(self){
        Question = @"";
        Answer = @"";
        card = 0;
        wrongAnswer = [NSArray arrayWithObjects:@"", @"",@"", nil];
    }
    
    return self;
}

- (void)setAnswer:(NSString *)inputAnswer
{
    Answer = inputAnswer;
}

- (void) setQuestion:(NSString *)inputQuestion
{
    Question = inputQuestion;
}
- (void) setWrongAnswer:(NSMutableArray *)inputwrongAnswer
{
    wrongAnswer = inputwrongAnswer;
}
- (void) setCard:(int)inputcard
{
    card = inputcard;
}


- (NSString*) getAnswer
{
    return Answer;
}

- (NSString*) getQuestion
{
    return Question;
}

- (NSMutableArray*) getwrongAnswer
{
    return wrongAnswer;
}

- (int) getCard
{
    return card;
}

@end
